package game;

public class Score {

    private double score = 100;
    private int correctWithinLimit = 0;
    private int correctAboveLimit = 0;
    private int wrong = 0;

    public synchronized double addTenPercent() {
        score += score * 10 / 100;
        return score;
    }

    public synchronized double addFivePercent() {
        score += score * 5 / 100;
        return score;
    }

    public synchronized void decrement() {
        score--;
    }

    public synchronized double value() {
        return score;
    }

    public synchronized int getCorrectWithinLimit() {
        return correctWithinLimit;
    }

    public synchronized void addCorrectWithinLimit() {
        this.correctWithinLimit++;
    }

    public int getCorrectAboveLimit() {
        return correctAboveLimit;
    }

    public void addCorrectAboveLimit() {
        this.correctAboveLimit++;
    }

    public synchronized int getWrong() {
        return wrong;
    }

    public synchronized void addWrong() {
        this.wrong++;
    }
}
