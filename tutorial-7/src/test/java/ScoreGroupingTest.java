import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class ScoreGroupingTest {

    private Map<String, Integer> scores;

    @Before
    public void setUp() {
        scores = new HashMap<>();

        scores.put("Alice", 12);
        scores.put("Bob", 15);
        scores.put("Charlie", 11);
        scores.put("Delta", 15);
        scores.put("Emi", 15);
        scores.put("Foxtrot", 11);
    }

    @Test
    public void testGrouping() {
        Map<Integer, List<String>> result = ScoreGrouping.groupByScores(scores);

        assertEquals(result.size(), 3);
    }

    @Test
    public void testMainMethod() {
        ScoreGrouping.main(new String[]{});
    }
}