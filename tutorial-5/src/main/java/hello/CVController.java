package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CVController {
    @GetMapping("/cv")
    public String cv(@RequestParam(name = "visitor", required = false)
                             String visitor, Model model) {
        if (visitor == null || visitor.equals("")) {
            model.addAttribute("title", "This is my CV");
        } else {
            model.addAttribute("title", visitor
                    + ", I hope you are interested to hire me");
        }

        return "cv";
    }
}
