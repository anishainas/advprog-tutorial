package tutorial;

import tutorial.javari.animal.Animal;
import tutorial.javari.animal.Condition;
import tutorial.javari.animal.Gender;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

public class CsvDatabase {

    public static final List<Animal> animalList = new ArrayList<Animal>();

    public static void load(String fileName) {
        Scanner scanner;

        try {
            BufferedReader reader = new BufferedReader(new FileReader("tutorial-9/animals_records.csv"));

            String line;
            while ((line = reader.readLine()) != null) {
                StringTokenizer tokenizer = new StringTokenizer(line, ",");
                animalList.add(new Animal(Integer.parseInt(tokenizer.nextToken())
                        , tokenizer.nextToken()
                        , tokenizer.nextToken()
                        , Gender.parseGender(tokenizer.nextToken())
                        , Double.parseDouble(tokenizer.nextToken())
                        , Double.parseDouble(tokenizer.nextToken())
                        , Condition.parseCondition(tokenizer.nextToken())));
            }
            reader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Animal remove(int id) {
        for (int x = 0; x < animalList.size(); x++){
            if (animalList.get(x).getId() == id){
                return animalList.remove(x);
            }
        }
        return null;
    }

    public static void add(String json) {
    }
}
