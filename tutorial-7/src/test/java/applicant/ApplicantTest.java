package applicant;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.function.Predicate;

import org.junit.Before;
import org.junit.Test;

public class ApplicantTest {

    private Applicant applicant;
    private Predicate<Applicant> qualifiedEvaluator;
    private Predicate<Applicant> creditEvaluator;
    private Predicate<Applicant> employmentEvaluator;
    private Predicate<Applicant> criminalRecordsEvaluator;

    @Before
    public void setUp() {
        applicant = new Applicant();
        qualifiedEvaluator = theApplicant -> theApplicant.isCredible();
        creditEvaluator = theApplicant -> theApplicant.getCreditScore() > 600;
        employmentEvaluator = theApplicant -> theApplicant.getEmploymentYears() > 0;
        criminalRecordsEvaluator = theApplicant -> !theApplicant.hasCriminalRecord();
    }

    @Test
    public void testEvaluateApplicant() {
        assertTrue((Applicant.evaluate(applicant, qualifiedEvaluator.and(creditEvaluator))));

        assertTrue(Applicant.evaluate(applicant, qualifiedEvaluator.and(creditEvaluator
                .and(employmentEvaluator))));

        assertFalse(Applicant.evaluate(applicant, qualifiedEvaluator.and(criminalRecordsEvaluator
                .and(employmentEvaluator))));

        assertFalse(Applicant.evaluate(applicant, qualifiedEvaluator.and(criminalRecordsEvaluator
                .and(creditEvaluator).and(employmentEvaluator))));
    }

    @Test
    public void testMainMethod() {
        Applicant.main(new String[]{});
    }
}
