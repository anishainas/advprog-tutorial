package sorting;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class SortAndSearchTest {

    private int[] sequence1;
    private int[] sequence2;
    private int[] correctSequence1;
    private int[] correctSequence2;

    @BeforeEach
    void setUp() {

        sequence1 = new int[]{5, 1, 9, 1, 6, 8, 19};
        sequence2 = new int[]{543, 1663, 62973, 1, 62, 6833, 1339,
            9, 8, 9, 4, 69, 68, 58, 480};
        correctSequence1 = new int[]{1, 1, 5, 6, 8, 9, 19};
        correctSequence2 = new int[]{1, 4, 8, 9, 9, 58, 62, 68, 69, 480, 543,
            1339, 1663, 6833, 62973};

    }

    @Test
    void quick_sort_test() {

        Sorter.quickSort(sequence1);
        for (int x = 0; x < sequence1.length; x++) {
            assertTrue(sequence1[x] == correctSequence1[x]);
        }

        Sorter.quickSort(sequence2);
        for (int x = 0; x < sequence2.length; x++) {
            assertTrue(sequence2[x] == correctSequence2[x]);
        }

    }

    @Test
    void slow_sort_test() {

        Sorter.slowSort(sequence1);

        for (int x = 0; x < sequence1.length; x++) {
            assertTrue(sequence1[x] == correctSequence1[x]);
        }


        Sorter.slowSort(sequence2);

        for (int x = 0; x < sequence2.length; x++) {
            assertTrue(sequence2[x] == correctSequence2[x]);
        }

    }

    @Test
    void binary_search_test() {

        Sorter.quickSort(sequence1);
        assertTrue(Finder.binarySearch(sequence1, 0, sequence1.length - 1, 50) == -1);
        assertTrue(Finder.binarySearch(sequence1, 0, sequence1.length - 1, 4) == -1);

        assertTrue(sequence1[Finder.binarySearch(sequence1, 0, sequence1.length - 1, 5)] == 5);
        assertTrue(sequence1[Finder.binarySearch(sequence1, 0, sequence1.length - 1, 9)] == 9);


    }

    @Test
    void slow_search_test() {

        Sorter.quickSort(sequence1);
        assertTrue(Finder.slowSearch(sequence1, 50) == -1);
        assertTrue(Finder.slowSearch(sequence1, 4) == -1);

        assertTrue(Finder.slowSearch(sequence1, 5) == 5);
        assertTrue(Finder.slowSearch(sequence1, 9) == 9);


    }

}
