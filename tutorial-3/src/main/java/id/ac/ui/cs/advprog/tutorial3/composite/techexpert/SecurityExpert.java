package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class SecurityExpert extends Employees {

    public SecurityExpert(String name, double salary) throws IllegalArgumentException {
        this.name = name;
        if (salary < 70000) {
            throw new IllegalArgumentException("salary must not be lower than $70000");
        }
        this.salary = salary;
        this.role = "Security Expert";
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
