package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DepokPizzaStoreTest {

    private PizzaStore depokPizzaStore;

    @BeforeEach
    void setUp() {
        depokPizzaStore = new DepokPizzaStore();
    }

    @Test
    void createPizza() {
        Pizza cheesePizza = depokPizzaStore.orderPizza("cheese");
        assertEquals(cheesePizza.getName(), "Depok Style Cheese Pizza");
        Pizza clamPizza = depokPizzaStore.orderPizza("clam");
        assertEquals(clamPizza.getName(), "Depok Style Clam Pizza");

        Pizza veggiePizza = depokPizzaStore.orderPizza("veggie");
        assertEquals(veggiePizza.getName(), "Depok Style Veggie Pizza");

    }
}