package tallycounter;

public class SynchronizedTallyCounter extends TallyCounter {
    private int synchronizedCounter = 0;

    @Override
    public synchronized void increment() {
        synchronizedCounter++;
    }

    @Override
    public synchronized void decrement() {
        synchronizedCounter--;
    }

    @Override
    public synchronized int value() {
        return synchronizedCounter;
    }
}
