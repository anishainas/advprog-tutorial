package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class MarinatedClam implements Clams {

    @Override
    public String toString() {
        return "Marinated Clam";
    }
}
