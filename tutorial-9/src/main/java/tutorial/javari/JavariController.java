package tutorial.javari;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.bind.annotation.*;
import tutorial.CsvDatabase;
import tutorial.javari.animal.Animal;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;
import static org.springframework.web.bind.annotation.RequestMethod.GET;


@RestController
public class JavariController {
    private final AtomicLong counter = new AtomicLong();

    @RequestMapping(value = "/javari", method = GET)
    public List<Animal> listAnimal(){

        if (CsvDatabase.animalList.size() > 0) {
            return CsvDatabase.animalList;
        } else {
            return null;
        }
    }

    @RequestMapping(value = "/javari/{id}", method = GET)
    public Optional<Animal> findAnimal(@PathVariable Integer id) {

        Optional<Animal> findAnimal = CsvDatabase.animalList.stream().filter(s -> s.getId().equals(id)).findFirst();
        if (!findAnimal.isPresent()) {
            return null;
        } else {
            return findAnimal;
        }
    }

    @DeleteMapping(value = "/javari/{id}")
    public Animal removeAnimal(@PathVariable Integer id) {
        Animal animal = CsvDatabase.remove(id);
        if (animal != null) {
            return animal;
        } else {
            return null;
        }
    }

    @PostMapping(value = "/javari/{JSON}")
    public Animal addAnimal(@PathVariable String JSON) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        Animal obj = mapper.readValue(JSON, Animal.class);
        CsvDatabase.animalList.add(obj);

        return obj;
    }



}
