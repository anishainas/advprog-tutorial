package sorting;


public class Sorter {

    /**
     * Some sorting algorithm that possibly the slowest algorithm.
     *
     * @param inputArr array of integer that need to be sorted.
     * @return a sorted array of integer.
     */

    public static int[] slowSort(int[] inputArr) {
        int temp;
        for (int i = 1; i < inputArr.length; i++) {
            for (int j = i; j > 0; j--) {
                if (inputArr[j] < inputArr[j - 1]) {
                    temp = inputArr[j];
                    inputArr[j] = inputArr[j - 1];
                    inputArr[j - 1] = temp;
                }
            }
        }
        return inputArr;
    }

    public static int[] quickSort(int[] sequence) {
        int i = 0;
        int j = 0;
        int left = 0;
        int right = sequence.length - 1;
        int stackPointer = -1;

        int[] stack = new int[128];
        int swap = 0;
        int temp = 0;
        while (true) {
            if (right - left <= 7) {
                for (j = left + 1; j <= right; j++) {
                    swap = sequence[j];
                    i = j - 1;
                    while (i >= left && sequence[i] > (swap)) {
                        sequence[i + 1] = sequence[i--];
                    }
                    sequence[i + 1] = swap;
                }
                if (stackPointer == -1) {
                    break;
                }
                right = stack[stackPointer--];
                left = stack[stackPointer--];
            } else {

                int median = (left + right) >> 1;
                i = left + 1;
                j = right;

                swap = sequence[median];
                sequence[median] = sequence[i];
                sequence[i] = swap;

                /* make sure: c[left] <= c[left+1] <= c[right] */
                if (sequence[left] > (sequence[right])) {
                    swap = sequence[left];
                    sequence[left] = sequence[right];
                    sequence[right] = swap;
                }
                if (sequence[i] > (sequence[right])) {
                    swap = sequence[i];
                    sequence[i] = sequence[right];
                    sequence[right] = swap;
                }
                if (sequence[left] > (sequence[i])) {
                    swap = sequence[left];
                    sequence[left] = sequence[i];
                    sequence[i] = swap;
                }
                temp = sequence[i];
                while (true) {
                    do {
                        i++;
                    } while (sequence[i] < (temp));

                    do {
                        j--;
                    } while (sequence[j] > (temp));

                    if (j < i) {
                        break;
                    }
                    swap = sequence[i];
                    sequence[i] = sequence[j];
                    sequence[j] = swap;
                }
                sequence[left + 1] = sequence[j];
                sequence[j] = temp;
                if (right - i + 1 >= j - left) {
                    stack[++stackPointer] = i;
                    stack[++stackPointer] = right;
                    right = j - 1;
                } else {
                    stack[++stackPointer] = left;
                    stack[++stackPointer] = j - 1;
                    left = i;
                }
            }
        }

        return sequence;
    }

}
