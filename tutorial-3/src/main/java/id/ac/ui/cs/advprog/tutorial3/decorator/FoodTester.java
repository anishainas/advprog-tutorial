package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class FoodTester {

    public static void main(String[] args) {

        //Thick Bun Burger with Beef Meat, Cheese, Cucumber, Lettuce, and Chili Sauce
        Food thickBunBurgerSpecial = BreadProducer.THICK_BUN.createBreadToBeFilled();

        thickBunBurgerSpecial = FillingDecorator.BEEF_MEAT.addFillingToBread(
                thickBunBurgerSpecial);

        thickBunBurgerSpecial = FillingDecorator.CHEESE.addFillingToBread(
                thickBunBurgerSpecial);

        thickBunBurgerSpecial = FillingDecorator.CUCUMBER.addFillingToBread(
                thickBunBurgerSpecial);

        thickBunBurgerSpecial = FillingDecorator.LETTUCE.addFillingToBread(
                thickBunBurgerSpecial);

        thickBunBurgerSpecial = FillingDecorator.CHILI_SAUCE.addFillingToBread(
                thickBunBurgerSpecial);

        System.out.println(thickBunBurgerSpecial.getDescription()
                + " costs $" + thickBunBurgerSpecial.cost());

        //No Crust Sandiwich with All Filling
        Food noCrustAllFillingSandwich = BreadProducer.NO_CRUST_SANDWICH
                .createBreadToBeFilled();

        noCrustAllFillingSandwich = FillingDecorator.BEEF_MEAT.addFillingToBread(
                noCrustAllFillingSandwich);

        noCrustAllFillingSandwich = FillingDecorator.CHEESE.addFillingToBread(
                noCrustAllFillingSandwich);

        noCrustAllFillingSandwich = FillingDecorator.CHICKEN_MEAT.addFillingToBread(
                noCrustAllFillingSandwich);

        noCrustAllFillingSandwich = FillingDecorator.CHILI_SAUCE.addFillingToBread(
                noCrustAllFillingSandwich);

        noCrustAllFillingSandwich = FillingDecorator.CUCUMBER.addFillingToBread(
                noCrustAllFillingSandwich);

        noCrustAllFillingSandwich = FillingDecorator.LETTUCE.addFillingToBread(
                noCrustAllFillingSandwich);

        noCrustAllFillingSandwich = FillingDecorator.TOMATO.addFillingToBread(
                noCrustAllFillingSandwich);

        noCrustAllFillingSandwich = FillingDecorator.TOMATO_SAUCE.addFillingToBread(
                noCrustAllFillingSandwich);

        System.out.println(noCrustAllFillingSandwich.getDescription()
                + " costs $" + noCrustAllFillingSandwich.cost());

        //Crusty Sandiwich with Beef Meat, Chicken Meat, Using Tomato and Chili Sauce
        Food doubleBeefChickenDoubleSauceSandwich =
                BreadProducer.CRUSTY_SANDWICH.createBreadToBeFilled();

        doubleBeefChickenDoubleSauceSandwich =
                FillingDecorator.BEEF_MEAT.addFillingToBread(
                        doubleBeefChickenDoubleSauceSandwich);

        doubleBeefChickenDoubleSauceSandwich =
                FillingDecorator.CHICKEN_MEAT.addFillingToBread(
                        doubleBeefChickenDoubleSauceSandwich);

        doubleBeefChickenDoubleSauceSandwich =
                FillingDecorator.CHILI_SAUCE.addFillingToBread(
                        doubleBeefChickenDoubleSauceSandwich);

        doubleBeefChickenDoubleSauceSandwich =
                FillingDecorator.TOMATO_SAUCE.addFillingToBread(
                        doubleBeefChickenDoubleSauceSandwich);

        System.out.println(doubleBeefChickenDoubleSauceSandwich.getDescription()
                + " costs $" + doubleBeefChickenDoubleSauceSandwich.cost());

    }

}
