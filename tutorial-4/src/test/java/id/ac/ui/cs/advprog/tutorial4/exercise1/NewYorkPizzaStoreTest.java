package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NewYorkPizzaStoreTest {

    private PizzaStore nyPizzaStore;

    @BeforeEach
    void setUp() {
        nyPizzaStore = new NewYorkPizzaStore();
    }

    @Test
    void createPizza() {
        Pizza cheesePizza = nyPizzaStore.orderPizza("cheese");
        assertEquals(cheesePizza.getName(), "New York Style Cheese Pizza");
        Pizza clamPizza = nyPizzaStore.orderPizza("clam");
        assertEquals(clamPizza.getName(), "New York Style Clam Pizza");

        Pizza veggiePizza = nyPizzaStore.orderPizza("veggie");
        assertEquals(veggiePizza.getName(), "New York Style Veggie Pizza");

    }
}