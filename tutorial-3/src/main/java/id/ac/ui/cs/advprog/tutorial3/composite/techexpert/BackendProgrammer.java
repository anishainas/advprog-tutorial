package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class BackendProgrammer extends Employees {

    public BackendProgrammer(String name, double salary) throws IllegalArgumentException {
        this.name = name;
        if (salary < 20000) {
            throw new IllegalArgumentException("salary must not be lower than $20000");
        }
        this.salary = salary;
        this.role = "Back End Programmer";
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
