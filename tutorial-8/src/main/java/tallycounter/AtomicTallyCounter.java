package tallycounter;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicTallyCounter extends TallyCounter {

    private AtomicInteger atomicCounter;

    public AtomicTallyCounter() {
        atomicCounter = new AtomicInteger();
    }

    @Override
    public void increment() {
        atomicCounter.getAndIncrement();
    }

    @Override
    public void decrement() {
        atomicCounter.getAndDecrement();
    }

    @Override
    public int value() {
        return atomicCounter.get();
    }
}
