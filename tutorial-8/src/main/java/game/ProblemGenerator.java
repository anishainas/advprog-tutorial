package game;

import java.util.Random;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;


public class ProblemGenerator implements Runnable {

    private static final int TOTAL_QUEST = 10;
    private static final int QUEST_TYPE_ADD = 0;
    private static final int QUEST_TYPE_SUBSTR = 1;
    private static final int QUEST_TYPE_MULTIPL = 2;
    private static final int QUEST_TYPE_DIVS = 3;
    private Thread thread;
    private int timeLeft;
    private Timer timer;
    private Score score;
    private int timeLimit;

    public ProblemGenerator(int timeLimit, Score score) {
        this.timer = new Timer();
        this.score = score;
        this.timeLimit = timeLimit;
    }

    public void start() {
        if (thread == null) {
            thread = new Thread(this);
            thread.start();
        }
    }

    @Override
    public void run() {
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                score.decrement();
            }
        }, 0, 1 * 1000);

        Scanner scanner = new Scanner(System.in);

        for (int questNo = 1; questNo <= TOTAL_QUEST; questNo++) {
            Timer problemTimer = new Timer();
            timeLeft = timeLimit;

            problemTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    if (timeLeft-- == 0) {
                        problemTimer.cancel();
                    }
                }
            }, 0, 1 * 1000);

            System.out.print(questNo + ") ");
            Random rand = new Random();
            Fraction firstPosFrac = new Fraction(rand.nextInt(40) - 20,
                    rand.nextInt(40) - 20);
            Fraction secondPosFrac = new Fraction(rand.nextInt(40) - 20,
                    rand.nextInt(40) - 20);
            Fraction expectedAnswer;

            switch (rand.nextInt(3)) {
                case QUEST_TYPE_ADD:
                    System.out.print(firstPosFrac.toString() + "  +  "
                            + secondPosFrac.toString() + "  =  ");
                    expectedAnswer = firstPosFrac.getAddition(secondPosFrac);
                    break;
                case QUEST_TYPE_SUBSTR:
                    System.out.print(firstPosFrac.toString() + "  -  "
                            + secondPosFrac.toString() + "  =  ");
                    expectedAnswer = firstPosFrac.getSubstraction(secondPosFrac);
                    break;
                case QUEST_TYPE_MULTIPL:
                    System.out.print(firstPosFrac.toString() + "  *  "
                            + secondPosFrac.toString() + "  =  ");
                    expectedAnswer = firstPosFrac.getMultiplication(secondPosFrac);
                    break;
                case QUEST_TYPE_DIVS:
                    System.out.print(firstPosFrac.toString() + "  :  "
                            + secondPosFrac.toString() + "  =  ");
                    expectedAnswer = firstPosFrac.getDivision(secondPosFrac);
                    break;
                default:
                    System.out.println("Oooops!");
                    expectedAnswer = new Fraction();
            }

            // Asking for question
            // And capture before and after the time in milis
            long totalMilis = System.currentTimeMillis();
            String rawAns = scanner.nextLine();
            totalMilis = System.currentTimeMillis() - totalMilis;

            // Process user answer
            Fraction userAnswer;
            if (rawAns.contains("/")) {
                String[] ans = rawAns.split("/");
                userAnswer = new Fraction(Integer.parseInt(ans[0]),
                        Integer.parseInt(ans[1]));
            } else {
                userAnswer = new Fraction(Integer.parseInt(rawAns));
            }


            // Check answer
            if (expectedAnswer.isEqual(userAnswer)) {
                System.out.println("Correct! Answered in: " + (timeLimit - timeLeft)
                        + " seconds, current score: " + calculateScore(timeLeft, true));
            } else {
                System.out.println("Wrong! Answered in: " + (timeLimit - timeLeft)
                        + " seconds, current score: " + calculateScore(timeLeft, false));
            }

            problemTimer.cancel();


        }

        int totalRightBelowThreshold = score.getCorrectWithinLimit();
        int totalRightAboveThreshold = score.getCorrectAboveLimit();
        int totalWrong = score.getWrong();

        // Print the result
        System.out.println("\n=========Result==========");
        System.out.println("Right answer and within time limit  =  "
                + totalRightBelowThreshold);
        System.out.println("Right answer but over time limit  =  "
                + totalRightAboveThreshold);
        System.out.println("Wrong answer  =  " + totalWrong);

        double totalPoint = score.value();

        System.out.println("\nTotal point acquired : " + totalPoint);

        System.out.println("\n");
    }

    private double calculateScore(int timeLeft, boolean isCorrect) {
        if (isCorrect && timeLeft >= 0) {
            score.addCorrectWithinLimit();
            return score.addTenPercent();
        } else if (isCorrect) {
            score.addCorrectAboveLimit();
            return score.addFivePercent();
        } else {
            score.addWrong();
            return score.value();
        }

    }
}
